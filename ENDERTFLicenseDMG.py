#! /usr/bin/env python
"""
This script adds an English and German license files to a DMG. Requires Xcode and rtf
license files.
Obviously only runs on a Mac.

Copyright (C) 2011-2013 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import tempfile
import optparse


class Path(str):
    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        os.unlink(self)


def mktemp(dir=None, suffix=''):
    (fd, filename) = tempfile.mkstemp(dir=dir, suffix=suffix)
    os.close(fd)
    return Path(filename)


def main(options, args):
    dmgFile, license, lizenz = args
    with mktemp('.') as tmpFile:
        with open(tmpFile, 'w') as f:
            f.write("""data 'TMPL' (128, "LPic") {
        $"1344 6566 6175 6C74 204C 616E 6775 6167"
        $"6520 4944 4457 5244 0543 6F75 6E74 4F43"
        $"4E54 042A 2A2A 2A4C 5354 430B 7379 7320"
        $"6C61 6E67 2049 4444 5752 441E 6C6F 6361"
        $"6C20 7265 7320 4944 2028 6F66 6673 6574"
        $"2066 726F 6D20 3530 3030 4457 5244 1032"
        $"2D62 7974 6520 6C61 6E67 7561 6765 3F44"
        $"5752 4404 2A2A 2A2A 4C53 5445"
};

data 'styl' (5000, "English") {
	$"0001 0000 0000 000E 0011 0015 0000 000C"
	$"0000 0000 0000"
};

data 'styl' (5001, "German") {
	$"0001 0000 0000 000E 0011 0015 0000 000C"
	$"0000 0000 0000"
};\n\n""")
            with open(license, 'r') as l:
                f.write('data \'RTF \' (5000, "English") {\n')
                for line in l:
                    if len(line) < 1000:
                        f.write('    "' + line.strip().replace('"', '\\"').replace('\\', '\\\\') +
                                '\\n"\n')
                    else:
                        for liner in line.split('.'):
                            f.write('    "' +
                                    liner.strip().replace('"', '\\"').replace('\\', '\\\\') +
                                    '. \\n"\n')
                f.write('};\n\n')
            with open(lizenz, 'r') as l:
                f.write('data \'RTF \' (5001, "German") {\n')
                for line in l:
                    if len(line) < 1000:
                        f.write('    "' + line.strip().replace('"', '\\"').replace('\\', '\\\\') +
                                '\\n"\n')
                    else:
                        for liner in line.split('.'):
                            f.write('    "' +
                                    liner.strip().replace('"', '\\"').replace('\\', '\\\\') +
                                    '. \\n"\n')
                f.write('};\n\n')
            f.write("""data 'LPic' (5000) {
	$"0000 0002 0000 0000 0000 0003 0001 0000"
};

data 'STR#' (5000, "English") {
	$"0006 0745 6E67 6C69 7368 0541 6772 6565"
	$"0844 6973 6167 7265 6505 5072 696E 7407"
	$"5361 7665 2E2E 2E7A 4966 2079 6F75 2061"
	$"6772 6565 2077 6974 6820 7468 6520 7465"
	$"726D 7320 6F66 2074 6869 7320 6C69 6365"
	$"6E73 652C 2070 7265 7373 20D2 4167 7265"
	$"65D3 2074 6F20 696E 7374 616C 6C20 7468"
	$"6520 736F 6674 7761 7265 2E20 4966 2079"
	$"6F75 2064 6F20 6E6F 7420 6167 7265 652C"
	$"2070 7265 7373 20D2 4469 7361 6772 6565"
	$"D32E"
};

data 'STR#' (5001, "German") {
	$"0006 0744 6575 7473 6368 0B41 6B7A 6570"
	$"7469 6572 656E 0841 626C 6568 6E65 6E07"
	$"4472 7563 6B65 6E0A 5369 6368 6572 6E2E"
	$"2E2E E74B 6C69 636B 656E 2053 6965 2069"
	$"6E20 D241 6B7A 6570 7469 6572 656E D32C"
	$"2077 656E 6E20 5369 6520 6D69 7420 6465"
	$"6E20 4265 7374 696D 6D75 6E67 656E 2064"
	$"6573 2053 6F66 7477 6172 652D 4C69 7A65"
	$"6E7A 7665 7274 7261 6773 2065 696E 7665"
	$"7273 7461 6E64 656E 2073 696E 642E 2046"
	$"616C 6C73 206E 6963 6874 2C20 6269 7474"
	$"6520 D241 626C 6568 6E65 6ED3 2061 6E6B"
	$"6C69 636B 656E 2E20 5369 6520 6B9A 6E6E"
	$"656E 2064 6965 2053 6F66 7477 6172 6520"
	$"6E75 7220 696E 7374 616C 6C69 6572 656E"
	$"2C20 7765 6E6E 2053 6965 20D2 416B 7A65"
	$"7074 6965 7265 6ED3 2061 6E67 656B 6C69"
	$"636B 7420 6861 6265 6E2E"
};\n""")
        os.system('hdiutil unflatten -quiet "%s"' % dmgFile)
        ret = os.system('%s -a %s -o "%s"' %
                        (options.rez, tmpFile, dmgFile))
        os.system('hdiutil flatten -quiet "%s"' % dmgFile)
        if options.compression is not None:
            os.system('cp %s %s.temp.dmg' % (dmgFile, dmgFile))
            os.remove(dmgFile)
            if options.compression == "bz2":
                os.system('hdiutil convert %s.temp.dmg -format UDBZ -o %s' %
                          (dmgFile, dmgFile))
            elif options.compression == "gz":
                os.system('hdiutil convert %s.temp.dmg -format ' % dmgFile +
                          'UDZO -imagekey zlib-devel=9 -o %s' % dmgFile)
            os.remove('%s.temp.dmg' % dmgFile)
    if ret == 0:
        print "Successfully added license to '%s'" % dmgFile
    else:
        print "Failed to add license to '%s'" % dmgFile

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.set_usage("""%prog <dmgFile> <licenseFileEN> <licenseFileDE> [OPTIONS]
  This program adds an English and German software license agreements to a DMG file.
  It requires Xcode and rtf text <licenseFileEN> <licenseFileDE>.

  See --help for more details.""")
    parser.add_option(
        '--rez',
        '-r',
        action='store',
        default='/Applications/Xcode.app/Contents/Developer/Tools/Rez',
        help='The path to the Rez tool. Defaults to %default'
    )
    parser.add_option(
        '--compression',
        '-c',
        action='store',
        choices=['bz2', 'gz'],
        default=None,
        help='Optionally compress dmg using specified compression type. '
             'Choices are bz2 and gz.'
    )
    options, args = parser.parse_args()
    cond = len(args) != 3
    if not os.path.exists(options.rez):
        print 'Failed to find Rez at "%s"!\n' % options.rez
        cond = True
    if cond:
        parser.print_usage()
        sys.exit(1)
    main(options, args)
