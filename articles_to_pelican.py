#!/usr/bin/env python
"""
This script converts a django-articles datadump into individual Markdown
files ready to be used in Pelican. To dump the file, navigate to your
django app directory (the one with manage.py), and enter the following
command:
    python manage.py dumpdata --format=json -n articles auth > articles.json

Then run this script with articles.json as the input file.

Requires requests, unidecode and simplejson.

Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import codecs

import requests
from simplejson import load
from unidecode import unidecode

MD = 'http://heckyesmarkdown.com/go/'


def main(inputFile, root):
    with open(inputFile, 'r') as f:
        articleList = load(f)
    authors = {}
    tags = {}
    statuses = {}
    attachments = {}
    articles = {}
    for item in articleList:
        model = item['model']
        if model == 'articles.tag':
            tags[item['pk']] = item['fields']['name']
        elif model == 'articles.articlestatus':
            statuses[item['pk']] = item['fields']['is_live']
        elif model == 'articles.attachment':
            if item['fields']['article'] not in attachments:
                attachments[item['fields']['article']] = []
            att = item['fields']['attachment']
            name = att.split('/')[-1]
            attachments[item['fields']['article']].append({'name': name, 'url': att})
        elif model == 'articles.article':
            articles[item['pk']] = item['fields']
        elif model in ('auth.group', 'auth.permission'):
            continue
        elif model == 'auth.user':
            authors[item['pk']] = item['fields']['username']
        else:
            raise NotImplementedError(model)
    always = False
    for key, article in articles.iteritems():
        slug = article['slug']
        file = os.path.join(root, '%s.md' % slug)
        if not always and os.path.exists(file):
            ans = raw_input('%s already exists. Overwrite? '
                            '[(Y)es|(n)o|(a)lways]: ' %
                            os.path.basename(file))
            if ans.lower()[0] == 'n':
                continue
            if ans.lower()[0] == 'a':
                always = True
        with codecs.open(file, 'w', 'utf-8') as f:
            f.write('Title: %s\n' % article['title'])
            f.write('Date: %s\n' % article['publish_date'])
            f.write('Author: %s\n' % authors[article['author']])
            f.write('Tags: %s\n' % ', '.join(
                [tags[x] for x in article['tags']]
            ))
            f.write('Slug: %s\n' % slug)
            f.write('Id: %d\n' % key)
            if attachments.get(key) is not None:
                f.write('Attachments: %s\n' % attachments[key])
            if not statuses[article['status']]:
                f.write('Status: draft\n')
            f.write('\n')
            req = requests.get(MD, params={'html': article['rendered_content']})
            if req.status_code == 200:
                f.write(req.text)
            else:
                f.write(article['content'])


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "Usage: %s <django articles json data> <output directory>" %\
              sys.argv[0]
        sys.exit(1)
    main(*sys.argv[1:])

