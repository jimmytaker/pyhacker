#! /usr/bin/env python
"""
This script creates an NFO file suitable to use with the Boxee Box by querying
the IMDB.

Requires mechanize.

Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import re
import sys
import cookielib
import htmlentitydefs

import mechanize

IMDB = 'http://www.imdb.com%s'

def debug(t, v, tb):
    import traceback, pdb
    traceback.print_exception(t, v, tb)
    print
    pdb.pm()

sys.excepthook = debug

def unescape(text):
    def fixup(m):
        text = m.group(0)
        if text[:2] == "&#":
            # character reference
            try:
                if text[:3] == "&#x":
                    return unichr(int(text[3:-1], 16))
                else:
                    return unichr(int(text[2:-1]))
            except ValueError:
                pass
        else:
            # named entity
            try:
                text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
            except KeyError:
                pass
        return text # leave as is
    return re.sub("&#?\w+;", fixup, text)

def getBrowser():
    br = mechanize.Browser()
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)
    br.set_handle_redirect(True)
    br.set_handle_equiv(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=5)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6')]
    return br

def getMovieID(name):
    imdb = 'http://www.imdb.com/find?s=all&q=%s'
    br = getBrowser()
    search = raw_input('Enter movie name [default=%s]: ' % name) or name
    br.open(imdb % search.replace(' ', '+'))
    soup = mechanize._beautifulsoup.BeautifulSoup(br.response().read())
    rows = [('%s %s' % (y('td')[-1].a.contents[0],
                        y('td')[-1].contents[-1].strip()),
             y('td')[-1].a.get('href'))
            for x in soup.body('table') for y in x('tr') if len(y('td')) >= 2
            and y('td')[-1].a.get('href', '').startswith('/title/')]
    showToIds = dict(rows)
    for i, show in enumerate(sorted(showToIds), 1):
        try:
            print u'%d. %s' % (i, unescape(show))
        except:
            pass
    while True:
        show = raw_input('Choose a movie: ')
        try:
            show = showToIds[sorted(showToIds)[int(show) - 1]]
            break
        except:
            pass
    return show

def writeNFO(response, filename, movieId):
    soup = mechanize._beautifulsoup.BeautifulSoup(response.read())
    details = soup.body('div', attrs=dict(id='maindetails_center_bottom'))[0]
    title = soup.body.h1.contents[0].strip()
    rating = soup.body('div', attrs={'class': 'star-box'})[0]('span', attrs={'class': 'value'})[0].contents[0]
    year = soup.body.h1.span.a.contents[0]
    try:
        plot = details('div', attrs={'class': 'article'})[1].p.contents[0].strip() or soup.body('p', attrs={'itemprop': 'description'})[0].contents[0].strip()
    except:
        plot = ''
    runtime = soup.body('div', attrs={'class': 'infobar'})[0].contents[0].split('-')[0].strip().replace('&nbsp;', '')
    director = soup.body('div', attrs={'class': 'txt-block'})[0].a.contents[0]
    names = [x.a.contents[0] for x in details('td', attrs={'class': 'name'})]
    characters = [x.div.contents[0].strip() or x.a.contents[0] for x in details('td', attrs={'class': 'character'})]
    actors = dict(zip(names, characters))
    txt = u"""<movie>
 <title>%s</title>
 <rating>%s</rating>
 <year>%s</year>
 <outline>%s</outline>
 <runtime>%s</runtime>
 <id>%s</id>
 <director>%s</director>
""" % (unescape(title), rating, year, unescape(plot), runtime, movieId, director)
    with open(filename, 'w') as f:
        f.write(txt)
        for name in sorted(names):
            f.write(' <actor>\n  <name>%s</name>\n  <role>%s</role>\n </actor>\n' % (name, actors[name]))
        f.write('</movie>\n\n')

def main(movie, name):
    br = getBrowser()
    br.open(IMDB % movie)
    movieId = movie.split('/')[-2]
    writeNFO(br.response(), '%s.nfo' % name, movieId)

if __name__ == '__main__':
    if len(sys.argv) not in (2, 3):
        print 'Usage: %s <movie file> [IMDB ID]' % sys.argv[0]
        sys.exit(1)
    name = os.path.splitext(sys.argv[1])[0]
    try:
        movieId = sys.argv[2]
    except:
        movieId = getMovieID(name)
    main(movieId, name)
