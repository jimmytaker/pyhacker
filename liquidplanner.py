#!/usr/bin/env python
"""
A Python interface to LiquidPlanner RESTful API. A full listing of available
API types can be found here: https://app.liquidplanner.com/api/help/types

Copyright (C) 2011 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import csv
import json
import urllib2
import optparse
from netrc import netrc
from datetime import date, datetime
from getpass import getpass

def debug(t, v, tb):
    import traceback, pdb
    traceback.print_exception(t, v, tb)
    print
    pdb.pm()

sys.excepthook = debug

API = "https://app.liquidplanner.com/api/"

def fetch(opener, relative_url):
    return json.load(opener.open(API + relative_url))

def plural(n, single='', plural='s'):
    """
    Return C{""} if n==1, otherwise return C{"s"}
    """
    if hasattr(n, '__len__'):
        n = len(n)
    return single if n == 1 else plural

def unlink(f):
    try:
        os.unlink(f)
    except:
        pass

def authenticate():
    try:
        n = netrc()
        username, account, password = n.authenticators('app.liquidplanner.com')
        if None in (username, password):
            raise
    except:
        username = raw_input('LiquidPlanner username: ')
        password = getpass('LiquidPlanner password: ')
        
    # set up HTTPS fetch function
    password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
    password_mgr.add_password(None, API, username, password)
    handler = urllib2.HTTPBasicAuthHandler(password_mgr)
    return urllib2.build_opener(handler), username

def getAccountInfo(opener):
    return fetch(opener, 'account')

def getWorkspace(opener, workspace=''):
    workspaces = fetch(opener, 'workspaces')
    if len(workspaces) < 1:
        print 'No workspaces found!'
        sys.exit(1)
    elif len(workspaces) == 1:
        return workspaces[0]
    names = [x['name'] for x in workspaces]
    if workspace in names:
        return workspaces[names.index(workspace)]
    for i, name in enumerate(names, 1):
        print '%d.\t%s' % (i, name)
    ans = raw_input('Select workspace [1-%d]: ' % i)
    return workspaces[int(ans) - 1]

def getMember(opener, wid, email):
    if email in ('None', None, 'none', '', 'all', 'All'):
        return {'id': None}
    for member in fetch(opener, 'workspaces/%d/members' % wid):
        if member['email'] == email:
            return member

def getMemberIdToEmailMap(opener, wid):
    idMap = {}
    for member in fetch(opener, 'workspaces/%d/members' % wid):
        idMap[member['id']] = member['email']
    return idMap

def getProjects(opener, wid):
    projects = {}
    for project in fetch(opener, 'workspaces/%d/projects' % wid):
        projects[project['id']] = project
    return projects

def getTasks(opener, wid):
    tasks = {}
    for task in fetch(opener, 'workspaces/%d/tasks' % wid):
        tid = task['id']
        if tid is not None:
            tasks[tid] = task
    return tasks

def getTimesheet(opener, workspace, email, startTime, endTime):
    wid = getWorkspace(opener, workspace)['id']
    mid = getMember(opener, wid, email)['id']
    projects = getProjects(opener, wid)
    tasks = getTasks(opener, wid)
    timesheet = fetch(opener, 'workspaces/%d/timesheet_entries' % wid)
    total = 0
    ps = {}
    startDate = datetime.strptime(startTime, '%Y-%m-%d')
    endDate = datetime.strptime(endTime, '%Y-%m-%d')
    for entry in timesheet:
        if entry['member_id'] != mid:
            continue
        performedOn = datetime.strptime(entry['work_performed_on'], '%Y-%m-%d')
        if performedOn >= startDate and performedOn <= endDate:
            iid = entry['item_id']
            t = tasks[iid]
            name = t['name']
            work = float(entry['work'])
            # only include items that have been submitted for review or accepted
            if entry['state'] not in ('accepted', 'submitted'):
                print 'Ignoring task with state "%s": %s (%d hours)' % (entry['state'], name, work)
                continue
            pid = t['project_id']
            project = ps.setdefault(pid, {'tasks': {},
                                          'name': projects[pid]['external_reference'] or '<blank>'})
            task = project['tasks'].setdefault(iid, {'entries': [],
                                                     'name': name,
                                                     'work': 0.0})
            task['entries'].append(entry)
            task['work'] += work
            total += work
    return ps, total

def flagsReport(opener, workspace, email, sendto):
    wid = getWorkspace(opener, workspace)['id']
    mid = [getMember(opener, wid, e)['id'] for e in email]
    midToEmail = getMemberIdToEmailMap(opener, wid)
    projects = getProjects(opener, wid)
    tasks = getTasks(opener, wid)
    ps = {}
    for task in tasks.values():
        if mid != [None] and task['owner_id'] not in mid:
            continue
        if task.get('alerts') == []:
            continue
        if task.get('done_on') is not None:
            continue
        pid = task['project_id']
        project = ps.setdefault(pid, {'tasks': [],
                                      'name': projects[pid]['external_reference'] or '<blank>'})
        project['tasks'].append(task)
    try:
        tmpFile = 'flagsReport.csv'
        msg = []
        rows = []
        for task in ps.values():
            msg.append(task['name'])
            for subtask in task['tasks']:
                #raise Exception
                owner = midToEmail.get(subtask['owner_id'], 'Unknown')
                msg.append('\t%s flag: %s (Owned by %s)' % (subtask['alerts'][0]['flag'].capitalize(), subtask['alerts'][0]['description'], owner))
                rows.append([task['name'], subtask['name'], owner, subtask['alerts'][0]['flag'], subtask['alerts'][0]['description']])
        rows.sort(key=lambda x: x[3])
        if 'None' in email:
            email.remove('None')
            email.append('everyone')
        if sendto != ['']:
            out = csv.writer(tmpFile)
            out.writerow(['Project Reference', 'Task', 'Task Owner', 'Flag Color', 'Flag Description'])
            out.writerows(rows)
            del out
            print 'Sending "flags" report to %s' % ', '.join(sendto)
            msg = [s.replace(r'"',r'\"') for s in msg]
            cmd = 'send_gmail.py %s -s"Flags report for %s" -a"%s" -m"%s\n\n%s"' %\
                      (','.join(sendto), ', '.join(email), tmpFile, ' '.join(sys.argv), '\n'.join(msg))
            os.system(cmd)
        else:
            print '\n'.join(msg)
    finally:
        unlink(tmpFile)

def promiseOnReport(opener, workspace, email, sendto):
    wid = getWorkspace(opener, workspace)['id']
    mid = [getMember(opener, wid, e)['id'] for e in email]
    midToEmail = getMemberIdToEmailMap(opener, wid)
    projects = getProjects(opener, wid)
    tasks = getTasks(opener, wid)
    ps = {}
    for task in tasks.values():
        if mid != [None] and task['owner_id'] not in mid:
            continue
        if task.get('promise_by') is None:
            continue
        if task.get('done_on') is not None:
            continue
        pid = task['project_id']
        project = ps.setdefault(pid, {'tasks': [],
                                      'name': projects[pid]['external_reference'] or '<blank>'})
        project['tasks'].append(task)
    try:
        tmpFile = 'promiseByReport.csv'
        msg = []
        rows = []
        for task in ps.values():
            msg.append(task['name'])
            for subtask in task['tasks']:
                owner = midToEmail.get(subtask['owner_id'], 'Unknown')
                msg.append('\t%s promised by %s (owned by %s)' % (subtask['name'], subtask['promise_by'], owner))
                rows.append([task['name'], subtask['name'], owner, subtask['promise_by']])
        rows.sort(key=lambda x: x[3])
        if 'None' in email:
            email.remove('None')
            email.append('everyone')
        if sendto != ['']:
            out = csv.writer(tmpFile)
            out.writerow(['Project Reference', 'Task', 'Task Owner', 'Promise Date'])
            out.writerows(rows)
            del out
            print 'Sending "promise on" report to %s' % ', '.join(sendto)
            msg = [s.replace(r'"',r'\"') for s in msg]
            cmd = 'send_gmail.py %s -s"Promise On report for %s" -a"%s" -m"%s\n\n%s"' %\
                      (','.join(sendto), ', '.join(email), tmpFile, ' '.join(sys.argv), '\n'.join(msg))
            os.system(cmd)
        else:
            print '\n'.join(msg)
    finally:
        unlink(tmpFile)

def timesheetReport(opener, workspace, email, sendto, startTime, endTime):
    timesheet, total = getTimesheet(opener, workspace, email, startTime, endTime)
    try:
        tmpFile = 'timesheet_%s.csv' % endTime
        header = ['Project Reference', 'Task', 'Date', 'Hours', 'Notes']
        msg = []
        rows = []
        for task in timesheet.values():
            msg.append(task['name'])
            for subtask in task['tasks'].values():
                msg.append('  %s (%s hour%s)' % (subtask['name'], subtask['work'], plural(subtask['work'])))
                for entry in subtask['entries']:
                    msg.append('    [%s] %s hour%s %s' % (entry['work_performed_on'], entry['work'],
                                                          plural(entry['work']), entry['note']))
                    rows.append([task['name'], subtask['name'], entry['work_performed_on'], entry['work'], entry['note']])
        if 'None' in email:
            email.remove('None')
            email.append('everyone')
        if sendto != ['']:
            out = csv.writer(tmpFile)
            out.writerow(['Timesheet %s - %s' % (startTime, endTime)])
            out.writerow(['Total hours: %s' % total])
            out.writerow(header)
            out.writerows(rows)
            del out
            print 'Sending timesheet to %s' % ', '.join(sendto)
            os.system('send_gmail.py %s -s"Timesheet %s - %s" -a"%s" -m"%s\n\n"' %\
                      (','.join(sendto), startTime, endTime, tmpFile,
                       'Timesheet %s - %s\n' % (startTime, endTime) + '\n'.join(msg) + '\nTotal hours: %s' % total))
        else:
            print 'Timesheet %s - %s' % (startTime, endTime)
            print '\n'.join(msg)
            print 'Total hours: %s' % total
    finally:
        unlink(tmpFile)

def main(options, args):
    opener, email = authenticate()
    if options.account_info:
        print 'Account info'
        for k, v in getAccountInfo(opener).iteritems():
            print '%s: %s' % (k, v)
        print
    # Timesheets
    sendto = options.timesheet_sendto.split(',')
    startTime = options.timesheet_start or options.timesheet_end
    endTime = options.timesheet_end
    if options.timesheet:
        timesheetReport(opener, options.workspace, email, sendto, startTime, endTime)
    # Reports
    sendto = options.report_sendto.split(',')
    if options.report_promiseon != '':
        email = options.report_promiseon.split(',')
        promiseOnReport(opener, options.workspace, email, sendto)
    if options.report_flags != '':
        email = options.report_flags.split(',')
        flagsReport(opener, options.workspace, email, sendto)

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.set_usage("""%prog [OPTIONS]
  A Python interface to LiquidPlanner RESTful API. A full listing of available
  API types can be found here: https://app.liquidplanner.com/api/help/types

  To use without being prompted for a password every time, create an entry in
  ~/.netrc that looks like:
     machine app.liquidplanner.com
     login username
     password password
  where username and password are your LiquidPlanner username and password. Make sure to chmod the file to
  600 so only you can read it (chmod 600 ~/.netrc)

  See --help for more details.""")
    parser.add_option('--account-info', action='store_true', default=False,
                      help="Retrieve account info")
    parser.add_option('--workspace', action='store', default='',
                      help="Set the workspace to interact with.")

    timesheet = optparse.OptionGroup(parser, 'Timesheet functions',
                                     "These functions allow for the export/manipulation of your timesheet.")
    timesheet.add_option('--timesheet-sendto', action='store', default='',
                         help="Specify an email (or comma separated list of emails) "
                              "to send the timesheet to.")
    timesheet.add_option('--timesheet', action='store_true', default=False,
                         help="""Retrieve timesheet""")
    timesheet.add_option('--timesheet-start', action='store', default='',
                         help="Specifies timesheet start date. Used in conjunction with the "
                              "--timesheet option. Date must be in YYYY-MM-DD format.")
    timesheet.add_option('--timesheet-end', action='store', default=date.today().strftime('%Y-%m-%d'),
                         help="Specifies timesheet start date. Used in conjunction with the "
                              "--timesheet option. Date must be in YYYY-MM-DD format.")
    parser.add_option_group(timesheet)

    reports = optparse.OptionGroup(parser, 'Misc reports',
                                   "These functions generate various reports.")
    reports.add_option('--report-sendto', action='store', default='',
                       help="Specify an email (or comma separated list of emails) "
                            "to send the report(s) to.")
    reports.add_option('--report-flags', action='store', default='',
                       help="Creates a report with all tasks that are flagged (red or yellow) "
                            "for the specified user(s). If --report-sendto is specified, "
                            "the report is emailed to that email or emails. To create "
                            "report for all users, set this variable to 'none'")
    reports.add_option('--report-promiseon', action='store', default='',
                       help="Creates a report with all tasks with a 'Promise on' date "
                            "set for the specified user(s). If --report-sendto is specified, "
                            "the report is emailed to that email or emails. To create "
                            "report for all users, set this variable to 'none'")
    parser.add_option_group(reports)

    options, args = parser.parse_args()
    if all(options.__dict__[k] == v for k, v in parser.defaults.iteritems()):
        parser.print_usage()
        sys.exit()
    main(options, args)
