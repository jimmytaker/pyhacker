#!/usr/bin/env python
"""
This script removes duplicate files from given directories.

Copyright (C) 2012 Jared Hobbs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
import os
import sys
import optparse
from hashlib import md5
from collections import defaultdict

from logutils import logger


def getFileMap(dir, files):
    fmap = defaultdict(list)
    for file in files:
        file = os.path.join(dir, file)
        if not os.path.isfile(file) or os.path.islink(file):
            continue
        fmap[os.path.getsize(file)].append(file)
    fmap2 = {}
    for k, v in fmap.iteritems():
        if len(v) > 1:
            fmap2[k] = v
    return fmap2


def computeHashes(fmap, bytes):
    fmap2 = defaultdict(list)
    for k, v in fmap.iteritems():
        for filename in v:
            with open(filename, 'rb') as f:
                fmap2[md5(f.read(bytes)).digest()].append(filename)
    return fmap2


def removeDuplicates(hashes, dryRun=False):
    for hash, files in hashes.iteritems():
        if len(files) == 1:
            continue
        logger.info("Keeping %s", files[0])
        for file in files[1:]:
            try:
                if not dryRun:
                    os.remove(file)
                    logger.info("Removed %s", file)
                else:
                    logger.info("Would have removed %s", file)
            except:
                logger.exception("Error removing %s", file)


def main(options, args):
    for dir in args:
        dir = os.path.abspath(dir)
        print
        logger.info('Scanning %s...', dir)
        if not os.path.isdir(dir):
            logger.warning("Skipping %s (not a directory)". os.path.basename(dir))
            continue
        files = os.listdir(dir)
        fileMap = getFileMap(dir, files)
        if not fileMap:
            logger.info("No files of equal size found in %s.", dir)
            continue
        del files
        hashes = computeHashes(fileMap, options.bytes)
        if not hashes:
            logger.info("No identical hashes found in %s.", dir)
            continue
        del fileMap
        removeDuplicates(hashes, dryRun=options.dry_run)
        del hashes


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.set_usage("""%prog [OPTIONS] <directory [directory ...]>
  This script removes duplicate files from a directory. It detects duplicates
  by computing a hash on the first ten kilobytes of the file.

  See --help for more information.
    """)
    parser.add_option('-b', '--bytes', action='store', type='int',
                      default=10240,
                      help='Sets the number of bytes to read from files of '
                           'the same length to hash. Defaults to %default')
    parser.add_option('-n', '--dry-run', action='store_true', default=False,
                      help="If set, don't actually delete duplicates; just "
                           "report duplicates to the terminal.")

    options, args = parser.parse_args()
    if len(args) < 1:
        parser.print_usage()
        sys.exit(1)
    main(options, args)

